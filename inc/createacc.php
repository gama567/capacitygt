<!DOCTYPE html>
<html lang="ES">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">	
	<link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="../css/util.css">
	<link rel="stylesheet" type="text/css" href="../css/main.css">
</head>
<body>
	<div class="limiter">
		<div class="container-login">
			<div class="wrap-login">
				<span class="login-form-title">
					Seleccione que tipo de cuenta registrará... 
				</span> 
				<div class="login-pic js-tilt" data-tilt >
					<a href="regempresa.php">
						<img src="../images/empresa.png" alt="IMG">
					</a>
				</div>
				<div class="login-pic js-tilt" data-tilt>
					<a href="regcandidato.php" >  
						<img src="../images/candidato.png" alt="IMG">	
					</a>
				</div>
				<br> <a href="login.php">VOLVER</a>
			</div>
		</div>
	</div>
	<script src="../vendor/jquery/jquery.min.js"></script>
	<script src="../vendor/bootstrap/js/popper.js"></script>
	<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="../vendor/select2/select2.min.js"></script>
	<script src="../vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
	<script src="../js/main.js"></script>
</body>
</html>