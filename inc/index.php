<?php

include("../util/system/funciones.php");
include("../util/system/session.php");
include("../util/system/conexion.php");

$conexion = new Conexion('util/logs/');
$conexion->conectar();
#Parametros del sistema
$session = new Session();

$resultado_parametros = $conexion->ejecutarConsulta("
SELECT * FROM Parametros");

$parametro = array();
foreach($resultado_parametros as $fila){
    $parametro[trim($fila['parametro'])] = trim($fila['valor']);
}
#Sesión abierta -> modulos del sistema
if($session -> checkSession()){

}else{ #Sesión no iniciada login.php
    include('login.php');
}
?>