<!DOCTYPE html>
<html lang="ES">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">	
	<link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="../css/util.css">
	<link rel="stylesheet" type="text/css" href="../css/main.css">
	
</head>
<body>
	<div class="limiter">
		<div class="container-login">
			<div class="wrap-login">
				<div class="login-pic js-tilt" data-tilt>
					<img src="../images/img-01.png" alt="IMG">
					<div class="text-center p-t-12">
						<span class="txt1">
							¿Desea volver a la 
						</span>
						<a class="txt2" href="index.php">
							página principal?
						</a>
					</div>
				</div>
				<form class="login100-form validate-form" id="formLogin">
				<div class="login-form validate-form">
					<span class="login-form-title">
						Inicio de sesión
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Su correo no coincide con los criterios ejemplo@extensión.com">
						<input class="input100" type="text" name="email" placeholder="Correo" id="txtCorreo" autocomplete="new-password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "Contraseña requerido">
						<input class="input100" type="password" name="pass" placeholder="Password" id="txtPassword">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					<div class="text-center p-t-12">
						<span class="txt1">
							¿Olvidó su
						</span>
						<a class="txt2" href="#">
							Contraseña?
						</a>
					</div>
					<div class="container-login-form-btn">
						<button type="submit" id="btn_submit" class="login-form-btn">
							Iniciar sesión
						</button>
					</div>
					<div class="text-center p-t-136">
						<a class="txt2" href="createacc.php">
							Crear una cuenta
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<?php include("index.php"); ?>
	<script src="../vendor/jquery/jquery.min.js"></script>
	<script src="../vendor/bootstrap/js/popper.js"></script>
	<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="../vendor/select2/select2.min.js"></script>
	<script src="../vendor/tilt/tilt.jquery.min.js"></script>
	<script src="../vendor/sweetalert2/sweetalert2.all.min.js"></script>

	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
	<script src="../js/main.js"></script>
</body>
<script type="text/javascript" language="javascript" src="../js/jsdata/login.js?v=<?php echo $parametro['webversion']; ?>"></script>

<script> txtCorreo.focus(); </script>
</html>