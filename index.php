<!DOCTYPE html>
<html lang="ES">

<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Cristhian">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <title>Capacity</title>
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/templatemo-lava.css">
    <link rel="stylesheet" href="css/owl-carousel.css">
</head>

<body>

    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->


    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="index.html" class="logo">
                            CAPACITY GT
                        </a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li class="scroll-to-section"><a href="#welcome" class="menu-item">Inicio</a></li>
                            <li class="scroll-to-section"><a href="#about" class="menu-item">Conócenos</a></li>
                            <li class="scroll-to-section"><a href="#contact-us" class="menu-item">Descripción</a></li>
                            <li > 
                                <a  href="inc/login.php">LOGIN</a>
                            </li>
                            
                       
                        </ul>
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->


    <!-- ***** Welcome Area Start ***** -->
    <div class="welcome-area" id="welcome">

        <!-- ***** Header Text Start ***** -->
        <div class="header-text">
            <div class="container">
                <div class="row">
                    <div class="left-text col-lg-6 col-md-12 col-sm-12 col-xs-12"
                        data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                        <h1>¿En búsqueda de un<em> EMPLEO?</em></h1>
                        <p>Bienvenido a CapacityGT nueva plataforma digital que facilita la búsqueda de oportunidades laborales y permite a reclutadores de entidades reconocidas promocionar sus ofertas laborales en una amplia comunidad de profesionales, enfocándose en promover las competencias laborales de los candidatos.</p> 
                        <a href="#about" class="main-button-slider">Conócenos</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>
    <!-- ***** Welcome Area End ***** -->

    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12"
                    data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <div class="features-item">
                        <div class="features-icon">
                            <h2>01</h2>
                            <img src="images/main/features-icon-1.png" alt="">
                            <h4>Competencias laborales</h4>
                            <p>Enfoque principal a evaluación de competencias laborales en los perfiles profesionales de cada uno de los candidatos.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12"
                    data-scroll-reveal="enter bottom move 30px over 0.6s after 0.4s">
                    <div class="features-item">
                        <div class="features-icon">
                            <h2>02</h2>
                            <img src="images/main/features-icon-2.png" alt="">
                            <h4>Sitio optimizado</h4>
                            <p>Ofrecemos gran seguridad a los datos confidenciales de cada de las personas que interactúan con el sistema.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12"
                    data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                    <div class="features-item">
                        <div class="features-icon">
                            <h2>03</h2>
                            <img src="images/main/features-icon-3.png" alt="">
                            <h4>Archivos en la nube</h4>
                            <p>Suba su documento a nuestro servidor web para que las entidades vean su perfil laboral únicamente a las ofertas laborales que aplique</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->

    <div class="left-image-decor"></div>

    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="promotion">
        <div class="container">
            <div class="row">
                <div class="left-image col-lg-5 col-md-12 col-sm-12 mobile-bottom-fix-big"
                    data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <img src="images/main/left-image.png" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
                <div class="right-text offset-lg-1 col-lg-6 col-md-12 col-sm-12 mobile-bottom-fix">
                    <ul>
                        <li data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                            <img src="images/main/about-icon-01.png" alt="">
                            <div class="text">
                                <h4>Búsqueda</h4>
                                <p>Plataforma con amplio banco de información que permite una amplia gestión de ofertas, candidatos y otros.</p>
                            </div>
                        </li>
                        <li data-scroll-reveal="enter right move 30px over 0.6s after 0.5s">
                            <img src="images/main/about-icon-02.png" alt="">
                            <div class="text">
                                <h4>Empleo</h4>
                                <p>Encuentra gran cantidad de ofertas laborales a las que puedas aplicar, cercanas a tu ubicación.</p>
                            </div>
                        </li>
                        <li data-scroll-reveal="enter right move 30px over 0.6s after 0.6s">
                            <img src="images/main/about-icon-03.png" alt="">
                            <div class="text">
                                <h4>Profesionales</h4>
                                <p>Nuestro sistema permite a las empresas reclutadoras abarcar una gran cantidad de participantes, acorde al puesto laboral con capacidad de gestión.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->

    <div class="right-image-decor"></div>

    <!-- ***** Testimonials Starts ***** -->
    <section class="section" id="testimonials">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="center-heading">
                        <h2>La plataforma donde encuentras <em>una gran cantidad de ofertas laborales.</em></h2>
                        <p>
                            Registráte en nuestra plataforma y ten accesso a información relevante de los últimos puestos laborales posteados a los que tendrás acceso y podrás aplicar desde la comodidad de tu hogar.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <footer id="contact-us">
        <div class="container">
            <div class="footer-content">
                <div class="row">
                    <!-- ***** Contact Form Start ***** -->
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="contact-form">
                            <form id="contact" action="" method="post">
                                <div class="row">
                                    <img src="images/main/cv.png" alt="">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- ***** Contact Form End ***** -->
                    <div class="right-content col-lg-6 col-md-12 col-sm-12">
                        <h2>Your<em>CapacityGT</em></h2>
                        <p>Nos sentimos complacidos de poder ayudar en los procesos de contratación de personal proveyendo de herramientas innovadoras que promueven la evaluación de candidatos enfocado en competencias laborales.
                            <br><br>Al mismo tiempo ayudamos a todo profesional a encontrar puestos vacantes a sus alrededores a los cuáles pueden aplicar a través de la plataforma.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="sub-footer">
                        <p>Copyright &copy; 2020 Your Capacity GT

                        | Designed by <a rel="nofollow" href="https://templatemo.com">Cristhian Esquivel</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- jQuery -->
    <script src="js/jquery-2.1.0.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugins -->
    <script src="js/owl-carousel.js"></script>
    <script src="js/scrollreveal.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imgfix.min.js"></script>
    <!-- Global Init -->
    <script src="js/custom.js"></script>
</body>
</html>