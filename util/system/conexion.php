<?php
class conexion{
    private $baseDatos;
    private $usuario;
    private $password;
    private $servidor;
    private $conexion;
    private $puerto;

    public function __construct( $logs )
    {
        $this->baseDatos='yourcapacitygt';
        $this->usuario='root';
        $this->password='Test12345.';
        $this->servidor='34.66.127.218';
        $this->puerto=3306;
    }
    public function parametros($baseDatos,$usuario,$password,$servidor, $puerto=3306){
        $this->baseDatos = $baseDatos;
        $this->usuario = $usuario;
        $this->password = $password;
        $this->servidor = $servidor;
        $this->puerto = $puerto;
    }
    public function conectar(){
        $mysqli = new mysqli($this->servidor, $this->usuario, $this->password, $this->baseDatos, $this->puerto);
        if($mysqli->connect_error){
            die("Error de conexión: ( ".$mysqli->connect_errno." ) ".$mysqli->connect_error);
            $this->conexion = false;
            return false;
        }else{
            $this->conexion = $mysqli;
            $this->conexion->set_charset('utf8');
            return true;
        }
    }
    public function ejecutarConsulta($sql){
        $resultado = $this->conexion->query($sql);
        if($resultado){
            return $resultado;
        }else{
            die("Error en query:( ".$this->conexion->error." )".$sql);
            return false;
        }
    }
    public function __destruct() //liberar recursos que solicita objecto
    {
        if($this->conexion){
            $this->conexion->close();
        }
    }
}

//Test de clase conexión






$conexion = new Conexion('../logs/');
$conexion->conectar();

$resultado = $conexion -> ejecutarConsulta("Select * from Usuario");
 /*
print_r($resultado->num_rows);
if($resultado->num_rows>0){
    foreach($resultado as $fila){
        echo "<pre>";
        print_r($fila);
        echo"</pre>";
    }
}*/


#$resultado = $conexion->ejecutarConsulta("UPDATE Usuario set password = 'Test12345.'");
#print_r($resultado);
